\documentclass[nonacm, sigconf]{acmart}
\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}

\setcopyright{none}
\settopmatter{printacmref=false}
\renewcommand\footnotetextcopyrightpermission[1]{}
\usepackage{enumitem}


\begin{document}

\title{Probabilistic protocol extensions for secure entry into public P2P networks}

\author{Alireza Rafiei}
\email{alireza@opuslogica.com}
\orcid{0000-0003-3216-8855}
\affiliation{%
  \institution{Opus Logica, Inc.}
  \streetaddress{901 Olive St.}
  \city{Santa Barbara}
  \state{California}
  \postcode{93101}
}

\begin{abstract}
Public P2P networks have a problem: how to let new participants join the network with high degree of security and privacy? Current approaches can be reduced to using hardcoded advertised nodes. This approach has various problems wherein hardcorded nodes are points of failure. No security guarantee could be given to either members of the network, or to the joining participant, as either party may be malicious. This paper presents a solution utilizing an existing Blockchain network as a service, and describes a set of probabilistic protocol extensions for structured overlay networks that protects both the member and the joining nodes.
\end{abstract}


\begin{CCSXML}
<ccs2012>
   <concept>
       <concept_id>10003033.10003106.10003114.10003115</concept_id>
       <concept_desc>Networks~Peer-to-peer networks</concept_desc>
       <concept_significance>500</concept_significance>
       </concept>
   <concept>
       <concept_id>10003033.10003083.10003014</concept_id>
       <concept_desc>Networks~Network security</concept_desc>
       <concept_significance>500</concept_significance>
       </concept>
   <concept>
       <concept_id>10003033.10003083.10003098</concept_id>
       <concept_desc>Networks~Network manageability</concept_desc>
       <concept_significance>300</concept_significance>
       </concept>
   <concept>
       <concept_id>10003033.10003083.10011739</concept_id>
       <concept_desc>Networks~Network privacy and anonymity</concept_desc>
       <concept_significance>500</concept_significance>
       </concept>
 </ccs2012>
\end{CCSXML}

\ccsdesc[500]{Networks~Peer-to-peer networks}
\ccsdesc[500]{Networks~Network security}
\ccsdesc[300]{Networks~Network manageability}
\ccsdesc[500]{Networks~Network privacy and anonymity}

\keywords{structured overlay networks, Peer-to-peer networks, network bootstrapping, blockchain}


\maketitle

\section{Introduction\label{intro}}
In the real world, a node within an overlay network can leave the network at
will at all times without regards to how its departure affects the network. A
node outside the network, however, cannot join the network if it has no current
knowledge of any live members of the network. It is not necessarily the case that
such lack of knowledge implies that a node has never been to the network: A node
who has previously left a network would have lost this knowledge if its cache is
either empty, or contains nodes who are no longer alive. On a broader scale, a
pathological network suffering from low activity \cite{PODC02} or high churn rate, may
partition. Algorithms for handling partitions either require that at least one
node can communicate to both partitions \cite{SASO07, Shafaat13}, or that all
nodes know the address of each other in the underlying network and thus can reconstruct
the overlay topology \cite{Jelasity09, Jelasity07}. New nodes
wishing to join a network, old nodes with outdated cache wishing re-entry, and
merging completely disconnected partitions all require an answer to the same
question: How can we have a node join an overlay network without current knowledge
of any members?

While churn and its impact on overlay networks is well studied \cite{Dia03,
  ATEC04, PODC02}, they are often
studied together and the join-side of churn, specifically, finding entry-nodes,
not only is less studied but also suffers from overloaded concepts and
terminology.

Given a node $n$ wishing to join an overlay network $N$ (for the reasons that will
become clear, we will call it \textit{applicant}), define an \textit{Entry node} of some
node $n\notin N$ to be the first $n^{\prime}\in N$ that should be contacted by
$n$, for $n$ to become a member of $N$ \footnote{Entry node of $n$ is not
  unique. Also note that Entry node of $n$ is not necessarily responsible for
  integrating $n$ into the overlay: For example in Chord \cite{chord} an entry
  node of $n$ will direct $n$ towards $succ(id(n))$.}. The node denoted by Entry
node of $n$, is often referred to as the \textit{Bootstrapping
  node} \cite{Dia03, ICPADS08, PERCOMW07, Lee11, SIGOPS03, Jelasity06, Cramer04, GDG08,
  marques14, CAN, Dabek01} but may also be referred to by other names such as
\textit{contact node} by \cite{Castro02}. The term Bootstrapping node however 
may also be used in the context of creating an overlay network from the ground
up \cite{Dia03, Lee11, ChoD05, Jelasity06, Dabek01}. For example, in \cite{Zave}
it’s shown that the Chord protocol cannot correctly be initialized with just one
node, and, depending on various parameters, the smallest possible collection of
nodes under which the protocol will behave correctly has a size greater than
one. We believe the term Bootstrapping node better reflects such conditions,
especially since algorithms dealing with creation of such overlay networks from
the ground up treat these two contexts interchageably. We will therefore prefer
usage of Entry Node due to its comparative unambiguity.

Furthermore, join-operations occuring in an open, decentralized P2P system
without a centralized authority are fundamentally different than those in a
closed system where the system administrator knows the address of every node.

The closed system addresses real problems \cite{G19003, skype07}, but by virtue of the system
administrator, all nodes within the underyling network have knowledge of
each other and therefore the problem of constructing an overlay network is a
problem in topology construction, which can be dealt with by either gossip
protocols \cite{ChoD05, Jelasity06} or even regular centralized join servers
\cite{Lee11} or other mechanisms \cite{wolinsky10}.

The join-mechanism for decentralized systems has also been studied, but none are satisfactory with our requirements. They are as follows:
\begin{enumerate}
  \item{\textbf{Privacy}}: A Join-mechanism should not expose an entry-node to public in order to prevent denial of service attacks and/or censorships, or just plain privacy.
  \item{\textbf{Incentive-Compatibility}}: A Join-mechanism should be an incentive-compatible \cite{incentive} mechanism for selfish nodes to engage and help new nodes join the network.
  \item{\textbf{Automaticity}}: A Join-mechanism should not require manual selection of entry nodes or any other form of manual maintenance by a centralized authority.
  \item{\textbf{Eclipse-resistence}}: A Join-mechanism should protect new nodes from Eclipse attacks: it should ensure with high probability, that the overlay network to which they enterred is indeed the correct one!
  \item{\textbf{Sybil-resistence}}: A Join-mechanism should protect the existing members of the network from Sybil attacks \cite{sybil} on the network.
  \item{\textbf{Federation}}: A Join-mechanism should be reachable by new nodes using a federated address i.e. the address should not point to a centralized server.
  \item{\textbf{Decentralization}}: A join-mechanism should not require code executions on a server owned by a trusted authority.
\end{enumerate}

To our knowledge no join-mechanism satisfies the requirements above
simultaneously. Some solutions are purely based on hard-coding entry nodes
\cite{bep9}. For the protocols like Tor \cite{tor} or Orchid \cite{orchid} whose security and privacy may not be compromised, such requirements are achieved by obfuscation \cite{Tor19}. Some solutions like \cite{GDG08, Cramer04} use the
bias in the location of users of various P2P systems \cite{DBLP, skype05} to
selectively sample the underlying network, and iteratively contact each node and
request a join. While for sufficiently large overlays, this approach will be
successful, none of the requirements are met. Other solutions meet the
federation requirement by using using D/DNS to point to a cache of nodes. This
usage of DNS is what is advocated by protocol designers such as \cite{CAN}. It often
involves hardcoding of trusted entry-nodes \cite{satoshi17, Miller15} with some
periodic updates. An example for an algorithm providing period updates is
\cite{ICPADS08} which updates the cache by having \textit{Guardian} nodes
check liveness of nodes within the cache. Besides DNS as a medium for porviding
entry nodes, other methods such as distribution through IRC or search
engines \cite{PERCOMW07}, or through social media \cite{marques14}
exist. Security or privacy mechanisms often require complex logic, and although
some work on secure admission control has been done \cite{Saxena03}, no solution
for satisfying the aforementioned requirements against the public exposure of the
entry nodes has been provided.

In this paper, we provide a join-mechanism that satisfies all the above
requirements. To do so, we will provide the join-mechanism by endowing the overlay
protocol with methods permitting nodes to interact with a smart contract on a
public blockchain \cite{eth, algorand, cardano}. We use a blockchain as a
trustless intermediary state machine between the overlay and the public. We will
use a simple probabilistic argument for demonstrating the eclipse-resistence of
this mechanism and constrain interactions of nodes inside of an overlay with a
node outside.

\section{System model and assumptions}
Applying protocol extensions of this paper to a given P2P structured overlay
network, constitutes a replacement of the protocol’s join operation, as well as
adding extra functionalities that do not supervene on the network. For this
reason, the only assumption that we make vis-a-vis the structure of the P2P
network, is that measures for preventing eclipse attacks by members of the
network against other members have been implemented. That is to say, any
pair of honest nodes can eventually communicate with each other. We will use
this assumption to provide confirmations to the recently joined nodes that they
have joined the correct network and not a simulated one (in the case where the
entry group is malicious). In other words, we concern ourselves only with the
security risks of entering into an unknown overlay network.

We structure the protocol extensions such that a correct node in the initial
overlay remains correct, without any modification after the application of these
extensions. As an example, in section \ref{prejoin}, we will need a way to select
a random sample of live participants of the network. A possible way is to
proceed as \cite{csar}, or more concretely, to implement this functionality
via verifiable random functions \cite{vrf}, as used in systems such as
\cite{algorand}. However, this requires all nodes to participate in the network,
since a correct behavior for a node that has been randomly selected is to
execute the request imposed on it. We will sketch a way whereby such selection
occurs without such impositions on correct nodes.

We assume that upon extending the protocol, the protocol will also be endowed
with a BFT consensus mechanism over the underlying network. This consensus
method is necessary for cases such as those participating in the bargaining game
discussed in section \ref{join}.

Since the systems we are targeting are public and permissionless, we do not
assume the existence of any certificate authority responsible for issuing unique
identifiers in order to prevent sybil attacks. Instead, we will provide sybil
resistance through economic means such as burning \cite{mitigation}.

We do assume the existence of a permissionless blockchain capable of running smart
contracts that is accessible by every potential applicant. Furthermore, every
node within the overlay network or wishing to join the overlay network has a
corresponding account on the blockchain, and the applicants can gather the
resources necessary to initiate an on-chain transaction. Additionally, we assume that every
node posseses public/private keys which may be different from those provided by the
blockchain. The purpose of such keys are to provide each node with a method of
asymmetric cryptography, as well as being the basis for a node’s identifier
within the network (or provide a deterministic way to derive the identifier of a
node with a given public key).

In order to avoid violating privacy requirements via traffic analysis, we assume
said blockchain is sufficiently active such that the ratio of transactions
related to the protocol extensions with respect to the total transactions per block is negligible.

On the fault model of the participants in the network, we assume the ratio
of the nodes exhibiting byzantine faults over all nodes, is bounded from above \cite{lamport}.
A subset of all nodes that choose to
participate in the join-mechanism which we will describe, exhibit utility
maximizing rational behavior insofar as it concerns the join operations
themselves. In other words, they behave rationally \cite{rational} for example,
concerning the cost of the bandwidth required to join other nodes into the
network, but will behave either correctly or faulty in any other aspect of the
the protocol. In addition, like \cite{bar}, we assume that rational nodes
benefit from long term operation of the network.

Finally, we assume the existence of at least one correct node (the
\textit{protocol developer}) who sets up the smart contracts with initial
parameters.

\section{Join Mechanism}
Consider an applicant $n$ who has declared interest in joining the
network. Furthermore, let $a$ be the node responsible for adding $n$ into the overlay
- in a structured overlay, given some metric on the network’s identifier space,
$id(a)$ is typically within a neighberhood of $id(n)$ (and often is the closest
to $n$). $n$ would be correctly placed in the network, if $a$ is not faulty.

Due to our assumption of operating on an overlay which already provides some
sort of protection against eclipse attacks - possibly by providing a constrained
routing table \cite{SIGOPS03} - we know with high probability that two correct
nodes could communicate without having their messages necessarily hop through a
faulty node. As a consequence, in steady state, any correct node should be able to
communicate with $a$ asking for the routing table of $n$ and get back the same
response.

In fact, let $E$ be a random sample of entry nodes (called \textit{Entry Group})
within the network which will contact $a$, asking for a routing table of
$n$. Assuming $E$ reaches consensus, if $E$ is either composed of all correct
nodes or is a coalition of faulty nodes, the following four cases are possible:

\begin{itemize}
  \item $a$ is faulty. $E$ is faulty.
  \item $a$ is correct. $E$ is faulty.
  \item $a$ is faulty. $E$ is correct.
  \item $a$ is correct. $E$ is correct.
\end{itemize}

In existing join-mechanisms that do not involve $n$ broadcasting to the underlying
network, $n$ cannot directly reach $a$, but it has to go through an intermediary
layer (like a DNS server) and request information of $a$ from the overlay
members who are present on the intermediary layer. Essentially those overlay
members will act as entry nodes for $n$. That is to say, $n$ will rely and act on
the information provided by an intermediary node $w$. $n$ will be correctly
placed into the network if both $n$ and $w$ are correct.

The mechanism that will be described here is somewhat similar insofar as $n$ having
to go through an intermediary layer to reach $a$. But that intermediary layer is a
smart contract which allows us to execute additional logic in order to account
for cases in which either $a$ or $E$ is faulty. Note that a correct $E$ can
agree on the proposed routing table of a faulty $a$, but it cannot validate the
correctness of the entries of the table: The overlay network could be under a
Sybil attack \cite{sybil}, and an adversary who controls $a$ and all the entries
of the proposed routing table, may behave correctly for all members of the overlay,
except $n$. That is to say, if the network is under Sybil attack, new nodes may
be eclipsed. Outbound communications of $n$ in the overlay should be treated as
suspect, until $n$ receives enough confirmations from enough randomly selected
nodes of the overlay as to whether $n$ is reachable by them. Note that this is
similar to the redundant routing of \cite{SIGOPS03}, and should be possible,
since as a direct consequence of the assmuption of overlay not being under
attack, some neighborhood of $n$ involves not all faulty nodes, which means not all
messages towards $n$ will hop through faulty nodes.


We will sketch the mechanism at four different stages described as follows:

\subsection{Pre-Join\label{prejoin}}
This is the stage at which an applicant interacts with the contract to declare
interest in joining the overlay, before any collaboration between the applicant
and its entry nodes happen. The process starts with applicant $n$ adding itself to
the list \textit{Applicants} on the contract. Such additions are required to have
a fixed cost $\mathfrak{C}_{applicants}$ in order to prevent spam traffic as a
consequence of malicious churn by $n$ \cite{security}.

By burning $\mathfrak{C}_{applicants}$ and joining \textit{Applicants}, $n$ can
call the function \textit{CallForEntryNodes}. \textit{CallForEntryNodes} causes
the smart contract to select $\left|E\right|$ many identifiers from the
identifier-space to form the set of Entry nodes $E$ for the node $n$. All the
randomly selected identifiers will be announced publicly on the contract.

From the time of the announcement, overlay members can call \textit{SelfNominate}
function to be considered as a candidate for $E$. Every call to
\textit{SelfNominate}, in addition to adding the callee to the list of nominees,
checks whether at least $\mathfrak{D}_{pre-join}$ many seconds has passed
since $n$ requested for entry nodes and if so, it checks whether at least
$\left|E\right|$ many nominees are present. If the two requirements are met,
then no more nominees are accepted, and contract picks the “closest” nodes with
the identifiers that are closest to the initial randomly selected identifiers. We
will discuss this part in section \ref{closeness}.

The process moves forward to the Join stage, only if $E$ is selected.

\subsection{Join\label{join}}
This is the stage at which the applicant $n$ and the Entry nodes $E$ are
matched. In order for this stage to move forward, $E$ must reach conesnsus on
the asking price $\mathfrak{p}\in\mathfrak{P}$ within $\mathfrak{D}_{consensus}$
seconds of the start of this stage such that $\inf\mathfrak{P}$ and
$\sup\mathfrak{P}$ are set by the protocol designer. Upon announcing
$\mathfrak{p}$, the applicant and Entry node enter into a bargaining game
\cite{rubinstein}, whereby if $n$ agrees with spending $\mathfrak{p}$ within
$\mathfrak{D}_{consensus}$ seconds since the initial announcement, $n$
announces agreement, otherwise $n$ will propose a counter-offer
$\mathfrak{p}^{\prime}\in\mathfrak{P}$ and await a decision from $E$. This process
continues until either party exits the game (possibly by timeout) or agrees on a
price like $\mathfrak{p}_{E}$.

In order to prevent infinite repetition of the case where a strategizing and
sufficiently patient adversary tries to match their applicant with an Entry
group that is also controlled by them, it is required that after reaching an
agreement on $\mathfrak{p}_{E}$, a fixed amount of $\mathfrak{C}_{join}$ will be
burned by $n$.

After mutual agreement on $\mathfrak{p}_{E}$, and $n$ burning
$\mathfrak{C}_{join}$, $n$ will send $\mathfrak{p}_{E}$ to the contract to be
held in escrow. $E$ will then contact the node which is responsible for adding
$n$, and request a routing table for $n$. $a$ will generate the table but will
not integrate $n$ into the overlay. $a$ then keeps the table in its cache,
then sends the table to $E$. If $E$ reaches consensus on the routing table, they
all sign and asymmetrically encrypt the table and information of $a$, against
$n$'s key and hand over the encrypted data to $n$ via the contract. Upon the
hand over, the $\mathfrak{p}_{E}$ is released from escrow and equally distributed
between $E$. On the other side, $n$ retrieves and decrypts the data sent by $E$,
communicates with $a$ and integrates itself into the overlay. Note that $n$
may get a defective or fictitious table if $E$ is all faulty. It is therefore
necessary to choose the size of $E$ to be large enough to reduce the chances of
total failure. We will discuss this choice in section \ref{size}.

\subsection{Pre-Confirmation}
This is the stage at which the newly joined applicant requests a confirmation on
whether it has joined the correct overlay network as represented by the
contract. Note that, as previously discussed, a faulty $a$ or possibly a faulty
$E$ may eclipse $n$ and redirect it into a completely different network. 

In order to raise confidence as to whether $n$ has entered into the correct
overlay represented by the contract, $n$ will burn a fixed amount
$\mathfrak{C}_{confirmation}$ to call the function
\textit{RequestingConfirmation} which triggers the selection of a random
identifier from the identifier space by the contract. Through the same
nomination process as the \textbf{pre-join} stage, the closest node to the
randomly selected identifier gets selected as the confirmation node $C$ and through
the bargaining game, $n$ will pay a price to $C$.

\subsection{Confirmation\label{conf}}
Once the confirmation process is initiated by $n$ and $C$, $C$ asymmetrically
encrypts a randomly selected plaintext and a randomly selected secret against
$n$'s key and sends it over the contract. Simultaneously, $C$ symmetrically
encrypts the selected plaintext with the selected secret and sends it to $n$
over the overlay multiple times (if available, redundant routing 
functionality should be used through constrained routing tables a la \cite{SIGOPS03}). As
previously discussed, if $n$ is correctly placed in the overlay and the overlay
itself is not under eclipse attack, $C$'s messages should eventually be
delivered to $n$. $n$ should inspect its incoming messages to see if any of them
can be decrypted using the secret received from $C$ over the contract, and if
so, whether it matches the plaintext. A match is a successful confirmation. $n$
should decide how many successful confirmation it needs to trust the network. If
the confirmations are scarce, $n$ leaves the network, generates a new
private/public key and therefore assumes a new identifier and subsequently
requests to join. $n$ will receive another randomly selected Entry group as well
as a new node who will add it based on its new identifier, if the network is
sufficiently dense. A more detailed discussion is on section \ref{count}.
\section{Technicalities}

\subsection{Size of the Entry Group\label{size}}
As we discussed in section \ref{prejoin}, the contract selects $\left|E\right|$
many random nodes. It is natural to ask how many is enough.

Since we're assuming an upper bound on the ratio of faulty nodes (call it
$\mathfrak{f}$), we can ask what the smallest sample size is, such that the
chances of the sample consisting of all faulty nodes is less than some desired
amount ($p_{f}$).

In fact, consider a network of size $N$ having $F$ faulty nodes (where
$\mathfrak{f}=\frac{F}{N}$). The probability of a sample of size $n$ having $k$
faulty nodes follows the Hypergeometric distribution \cite{hyper}.

Let $X\sim Hypergeometric(N,F,x)$ where $x$ is the sample size. The question
above is asking for the smallest value of $x$ such that:

\begin{displaymath}
  \mathbb{P}(X=x)<p_f
\end{displaymath}

Note that:
$$\begin{aligned} \mathbb{P}(X=x)=\cfrac{\binom{F}{x}\binom{N-F}{x-x}}{\binom{N}{x}}=\cfrac{\binom{F}{x}}{\binom{N}{x}}\end{aligned}$$
And from elementary combinatorics the following is true for all $n,k\in\mathbb{N}$ given $n>k$:
$$\begin{aligned} \cfrac{n^{k}}{k^{k}}<\binom{n}{k}\end{aligned}$$
Therefore, given $\mathfrak{f}=\cfrac{F}{N}$:
$$\mathfrak{f}^{x}<P(X=x)<p_{f}$$
which gives:
$$\begin{aligned} x>\cfrac{\ln p_{f}}{\ln\mathfrak{f}}\end{aligned}$$
Since $x$ is an integer:

$$x=\left\lceil \cfrac{\ln p_{f}}{\ln\mathfrak{f}}\right\rceil$$

\subsection{Nominees, Closeness and the formation of Entry groups\label{closeness}}

As previously discussed in section \ref{prejoin}, nodes within the overlay can call
\textit{SelfNominate} function to nominate themselves to be part of the Entry
group $E$, where $\|E\|$ randomly selected identifiers were chosen and announced
by the contract. It was stated that the closest nominees to the randomly selected
identifiers will become part of $E$.

\begin{proposition}
Let $X$ be a metrizable identifier-space of the overlay with some metric
$d$. Let $I\subset X$ be the set of randomly selected identifiers, and let
$N \subset X$ be the set of available nominees such that $\|N\| \geq \|I\|$.
Finding the closest subset of nominees $M \subseteq N$ to $I$ where
$\|M\|=\|I\|$ can be done in polynomial time.
\end{proposition}

\begin{proof}
Construct a weighted complete bipartite graph $G:=(I,N,E)$ where $E=\{(i,n):i\in
I,n\in N\}$ weighted on $\left\Vert d(.,.)\right\Vert$.
Finding the closest subset of nominees $M\subseteq N$ to $I$ where
$\left|M\right|=\left|I\right|$, is equivalent to finding a matching $M$ on $G$
where the weights are minimized.

Let $I^{\prime}=I\cup J$ with some $J\subset X$ such that
$\left|J\right|=\max(0, \left|N\right| - \left| I\right|)$ and $\min\{\left\Vert
  d(j,n)\right\Vert :j\in J,n\in N\}>\underset{i\in I,N\in N}{\sum}\left\Vert
  d(i,n)\right\Vert$ .

Finding the minimum weight matching on
$G^{\prime}=(I^{\prime},N,E^{\prime})$ where $E^{\prime}$ is the set of edges
between $I^{\prime}$ and $N$ is an assignment problem solvable in polynomial
time, and can be done in $O(n^3)$ using the Hungarian algorithm \cite{assignment}.

Let $M^\prime$ be such a matching on $G^\prime$. Then $M\subseteq M^{\prime}$ is
a minimum weighted matching on $G$ if there is no edge $e \in M$ such that $e$
is incident to any element of $J$. This claim is supported by the definition of
$J$, since the weight of any edge incident to an element of $J$ is greater than
the maximum weight of $G$.

\end{proof}


\subsection{Confirmation count\label{count}}
As previously discussed in section \ref{conf}, a newly joined applicant should request
confirmations from members who are part of the overlay. But how many
confirmations are necessary, and how many of them should be successful to imply
the applicant has joined the correct network? That is to say, assuming that one
has some prior belief about correctness of existing nodes with an upper limit on
the ratio of the faulty nodes, how to say whether a given sample of nodes is
enriched towards the faulty direction?

Note that the sample size itself (i.e. total number of requested confirmations)
is dependent on how much a new node is willing to spend, since the higher the
sample size, the more accurately statistical significance can be measured.

Recall that for a network of size $N$ having $F$ faulty nodes (where
$\mathfrak{f}=\frac{F}{N}$), the probability of a sample of size $n$ having $k$
faulty nodes follows the Hypergeometric distribution. Since $N \gg n$, we can
approximate the Hypergeometric random variable with a binomial distribution
where $ p=\mathfrak{f}$.

Let $X\sim Binomial(n,\mathfrak{f})$ with the probability mass function:

\begin{displaymath}
  \mathbb{P}(X=k)=\binom{n}{k}\mathfrak{f}^{k}(1-\mathfrak{f})^{n-k}
\end{displaymath}
where $k$ denotes the number of unsuccessful confirmations in a sample of size $n$.

The sample is not enriched towards faulty nodes if given a significance level
$\alpha$, we have:

\begin{equation}
1 - \alpha < \mathbb{P}(X=k)\label{main}
\end{equation}

\begin{proposition}
A rule of thumb for a network with $\mathfrak{f}=0.33$ and $\alpha=99.99$ is to trust the network if no more
than $\lfloor 0.41n \rfloor$ of the confirmations are failing.
\end{proposition}

\begin{proof}

Our approach is first to derive an upper bound for $k$ in \ref{main}
fixing $\alpha$, then to conduct linear regression with 1000 datapoints wherein
$n$ is the independent variable and $k$ is the dependent variable.

In order to derive the upper bound, Note that:
\begin{equation}
\begin{aligned}
\mathbb{P}(X\geq k) & = 1 - \mathbb{P}(X \leq k) \\
 & = 1 - I_{1-\mathfrak{f}}(n-k,k+1) \\
\end{aligned}
\end{equation}
where $I_{p}(.,.)$ is the regularized incomplete beta function
\cite{bindist}.

Using \textit{SCIPY} package \cite{scipy} v. 1.3.1 we calculated the datapoints
and using \textit{scikit-learn} package \cite{scikit-learn}
v. 0.21.3 derived the regression line minimizing residual sum of squares, which
has an intercept of $b_0\approx 2.6$ and coefficient of $b_1 \approx 0.41$.

\end{proof}

\subsection{Differential policy for nodes who have never been on the overlay versus those with outdated peer-cache}
Consider a node $n$ in an overlay that receives a message from another party
$m$. How should $n$ react? Should $n$ react differently if $m$ has never been
part of the overlay? How can $n$ distinguish between $m$ having not ever been a
member in the overlay versus $m$ having been a member before but due to churn or
other reasons, has an outdated peer-cache? In an unextended overlay protocol, $n$
doesn't react differently and if $m$'s message is a request for joining the
overlay, $n$ integrates $m$ into the overlay by either doing so itself or by
sending $m$ the information of the node responsible for adding $m$. 

In the extended version of the overlay, if we accept a cost of requiring every
node to have a join-record on the contract, we can distinguish between a new $m$
and an $m$ with an outdated peer-cache and any node within the overlay can accept
an outsider $m$ only if it has been part of the overlay before, but now has
outdated peer-cache. 

Note that such a distinction is possible because the join-mechanism on a contract
is accountable \cite{accountability}: Assuming with high probability that the
blockchain prevents double-spending \cite{doublespend}, the records of
successful joins are non-repudiable, publicly auditable and are stored in a way
that is tamper-evident. For accountable systems, fault-detection become conducive to
automated detections mechanisms such as \cite{SADFE08} and specifically in our case,
detection and assigning blame for \textit{faults of comission} is possible
\cite{OPODIS09}. In other words, all the correct nodes in the overlay will have
an auditable record of all members who have previously joined the
network. Consequently, nodes within the overlay can incorporate a differential
policy whereby if they receive a message from any node who does not have a join
record on the contract, they can reject the message, and direct the sender to go
throught the protocol before attempting to send messages on the network.

Note that some best practices for maintanence of accountable systems,
specifically truncation of logs after fixed periods of time (typically on the
order of months), tailored for an overlay \cite{peerreview} induce some form of
Sybil resistence for extended overlays using the policy above.

Also, note requiring payment is a mechanism to defend against spamming on the
contract. It does not provide a defense against Sybil attacks for the overlay
network itself, since whatever a Sybil attacker had to pay to have her node join
the network, she can make back by participating in the join-mechanism and have a
self-financing strategy to saturate the network with her nodes\footnote{Recall
  that, just like most open P2P networks, we do not assume the existence of a
  centralized certificate authority responsible for issuing unique
  identifiers.}. Assuming a fixed reproduction rate (where reproduction rate is
the rate at which the attacker can make up the amount spent on joining the
network with amounts made from participating in having others join), an attacker
will have a fixed amount of time to populate the network until the logs are cleared.

\section{Conclusion}
The requirements of the join protocol we described in section \ref{intro} were as
follows:

\begin{enumerate}
\item{\textbf{Privacy}}: This item is satisfied by virtue of having applicants
  and members of the overlay interact on-chain, and not handing over the routing
  table without verifying that the applicant is not malicious.
\item{\textbf{Incentive-Compatibility}}: This item is satisfied by making
  participation in the join protocol optional, as well as having the applicant
  and the entry group play a bargaining game wherein they have complete freedom
  in setting prices and accepting or rejecting outcomes.
\item{\textbf{Automaticity}}: This item is satisfied, since the protocol
  developer only had to set up the smart contract representing the overlay, and
  they did not hardocde any subset of nodes of the network.
\item{\textbf{Eclipse-resistence}}: This item is satisfied, since assuming the
  network is not currently under an eclipse attack and measures for preventing
  eclipse attacks by the existing members of the network have been implemented,
  new nodes can determine whether or not they faced eclipse attack and entered
  into a malicious alternative network, by evaluating the ratio of failed
  confirmations over total confirmations requested, as described in section \ref{conf}.
\item{\textbf{Sybil-resistence}}: This item is satisfied, since it's costly for
  new nodes to request entry as well as indefinitely continuing the bargaining game.
\item{\textbf{Federation}}: This item is satisfied, since the address of the
  smart contract identifying the network, which both applicants and overlay network
  communicate over, is fixed, and not directly tied to any member of the
  network except the always correctly behaving protocol developer.
\item{\textbf{Decentralization}}: This item is satisfied, simply by the virtue
  of the blockchain responsible for code execution, being a decentralized
  replicated state machine.
\end{enumerate}

\section{Future Work}

We identify the following avenues for improvements and future work:

First, Blockchain systems are still at their infancy, with some of the large
projects suffering from serious scalability issues and consequently transaction
costs \cite{scalability}. These issues prevents applying techniques of this
paper to networks with high join-rate as well as cheaply computing some aspect
of these extensions like those of section \ref{closeness}. Investigation and
development on more efficient and cheap blockchain systems are necessary for our
techniques to be widely applicable.

Second, the satisfaction of our requirements is not clear, if the
extended network itself is operating on a separate chain or a side-chain or fork
of the utilized public blockchain. For example, we might lose some anonymity
through traffic analysis, but gain more by having a structured and verifiable
mechanism for bargaining. More work here is necessary to evaluate the effect of
extending blockchain networks with our extensions.

Third, the fault model of this paper limited rational behavior only to the
join-mechanism aspect of a protocol, and delegated the analysis to the existing
and well respected literature in economics. Computational aspects of game theory
and mechanism design are still a largely unknown territory with many impossibility
results \cite{damd, agt10}, even though some recent advances potentially
applicable to our techniques have been made \cite{FOCS13}. More work is necessary to
strengthen rational behavior exhibited by members of the extended network.

Fourth, it is highly desireable for the extensions described in this paper to be
improved in terms of their efficiency. For example, the scheme we describe for
selecting random samples resolves in $O(n^3)$ time complexity on-chain (where
$n$ is the number of nominees). More work is necessary to either
improve the efficiently of this scheme, or develop other schemes that are still
optional to participate for current members of the extended network.

\begin{acks}
  We thank Brian J. Fox from the Orchid protocol for their input and support.
\end{acks}

\bibliographystyle{ACM-Reference-Format}
\bibliography{refs}


\end{document}
\endinput
