SHELL := /bin/bash
PATH  := $(PATH):/Library/TeX/texbin

TEX_SOURCES = $(wildcard *.tex) $(wildcard */*.tex)
BIB_SOURCES = $(wildcard *.bib) $(wildcard */*.bib)
DOCUMENT    = paper
DRAFT       = draft

$(DOCUMENT).pdf: $(TEX_SOURCES) $(BIB_SOURCES)
	rm -f $(DOCUMENT).pdf
	pdflatex $(DOCUMENT)
	bibtex $(DOCUMENT)
	pdflatex $(DOCUMENT)
	pdflatex $(DOCUMENT)

$(DRAFT).pdf: $(TEX_SOURCES) $(BIB_SOURCES)
	rm -f $(DRAFT).pdf
	pdflatex $(DRAFT)
	bibtex $(DRAFT)
	pdflatex $(DRAFT)
	pdflatex $(DRAFT)

$(DRAFT): $(DRAFT).pdf

clean: FORCE
	rm -rf `find . -name "*.aux" -o -name "*.blg" -o -name "*.dvi" -o -name "*.log" -o -name "*.lot" -o -name "*.lof" -o -name "*.toc" -o -name "*.bbl" -o -name "*.bcf" -o -name "*.run/xml"`

distclean: clean
	rm -f $(DOCUMENT).pdf $(DRAFT).pdf

FORCE:
